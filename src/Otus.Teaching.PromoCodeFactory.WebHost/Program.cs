using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

var builder = WebApplication.CreateBuilder(args);


//builder.Services.AddScoped(typeof(IRepository<Employee>), (x) => new InMemoryRepository<Employee>(FakeDataFactory.Employees));
//builder.Services.AddScoped(typeof(IRepository<Role>), (x) => new InMemoryRepository<Role>(FakeDataFactory.Roles));
//builder.Services.AddScoped(typeof(IRepository<Preference>), (x) => new InMemoryRepository<Preference>(FakeDataFactory.Preferences));
//builder.Services.AddScoped(typeof(IRepository<CustomerPreference>), (x) => new InMemoryRepository<CustomerPreference>(FakeDataFactory.CustomerPreference));
//builder.Services.AddScoped(typeof(IRepository<Customer>), (x) => new InMemoryRepository<Customer>(FakeDataFactory.Customers));

builder.Services.AddTransient<ICustomerRepository, CustomerRepository>();
builder.Services.AddTransient<IPreferenceRepository, PreferenceRepository>();
builder.Services.AddTransient<IPromoCodeRepository, PromocodesRepository>();
builder.Services.AddTransient<IEmployeeRepository, EmployeeRepository>();

builder.Services.AddDbContext<OtusDbContext>(optionsBuilder=> optionsBuilder
       .UseSqlite(builder.Configuration.GetConnectionString("DefaultConnection")));

builder.Services.AddControllers();
builder.Services.AddOpenApiDocument(options =>
{
    options.Title = "PromoCode Factory API Doc";
    options.Version = "1.0";
});

var app = builder.Build();
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseHsts();
}

using (var scope = app.Services.CreateScope())
{
    var dbContext = scope.ServiceProvider
        .GetRequiredService<OtusDbContext>();
    //�������� ������
    //dbContext.Database.EnsureDeleted();
    dbContext.Database.EnsureCreated();
    //���������� ������
    //DataSeed.SeedData(dbContext);
    //�������� ���������
    //dbContext.Database.Migrate();
}


app.UseOpenApi();
app.UseSwaggerUi3(x =>
{
    x.DocExpansion = "list";
});

app.UseHttpsRedirection();

app.UseRouting();
app.MapControllers();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
});

app.Run();


