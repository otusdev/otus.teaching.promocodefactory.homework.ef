﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController : ControllerBase
    {
        private readonly IPromoCodeRepository _promoCodeRepository;
        private readonly ICustomerRepository _customerRepository;
        private readonly IEmployeeRepository _employeeRepository;
        public PromocodesController(IPromoCodeRepository promoCodeRepository, ICustomerRepository customerRepository,
           IEmployeeRepository employeeRepository)
        {          
            _customerRepository = customerRepository;
            _employeeRepository = employeeRepository;
            _promoCodeRepository = promoCodeRepository;
        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {          
            var promoCodes = await _promoCodeRepository.GetAllAsync();
            var resulteList = new List<PromoCodeShortResponse>();
            foreach (var promoCode in promoCodes)
            {
                resulteList.Add( new PromoCodeShortResponse()
                {
                    Id = promoCode.Id,
                    PartnerName = promoCode.PartnerName,
                    Code = promoCode.Code,
                    BeginDate = promoCode.BeginDate.ToString("dd.MM.yyyy"),
                    EndDate = promoCode.EndDate.ToString("dd.MM.yyyy"),
                    ServiceInfo = promoCode.ServiceInfo,
                });
            }
            return Ok(resulteList);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            if (request is null)
            {
                return BadRequest("Request is empty");
            }
            var customers = await _customerRepository.GetAllAsync(c => c.CustomerPreferences.Any(p => p.Preference.Id == new Guid(request.Preference)));
            var employee = await _employeeRepository.GetEmployee(e => (e.FirstName + " " + e.LastName) != "");
            if (customers.Count > 0)
            {
                var promocodes = new List<PromoCode>();
                foreach (var customer in customers)
                {
                    var promoCode = new PromoCode()
                    {
                        ServiceInfo = request.ServiceInfo,
                        Code = request.PromoCode,
                        PartnerName = request.PartnerName,
                        Id = Guid.NewGuid(),
                        CustomerId = customer.Id,
                        PreferenceId = new Guid(request.Preference),
                        PartnerManagerId = employee.Id,
                        BeginDate = DateTime.Now,
                        EndDate = DateTime.Now.AddDays(7),
                    };
                    promocodes.Add(promoCode);
                }
                await _promoCodeRepository.CreateList(promocodes);
            }
            else
            {                
                await _promoCodeRepository.AddAsync(new PromoCode()
                {
                    ServiceInfo = request.ServiceInfo,
                    Code = request.PromoCode,
                    PartnerName = request.PartnerName,
                    Id = Guid.NewGuid(),   
                    PreferenceId = new Guid(request.Preference)
                });
            }
            return Ok();
        }
    }    
}