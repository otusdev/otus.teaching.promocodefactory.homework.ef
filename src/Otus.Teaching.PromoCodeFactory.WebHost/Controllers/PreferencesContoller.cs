﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesContoller : ControllerBase
    {
        private readonly IPreferenceRepository _preferenceRepository;
        public PreferencesContoller(IPreferenceRepository preferenceRepository)
        {
            _preferenceRepository = preferenceRepository;
        }
        // <summary>
        /// <summary>
        ///Получить список предпочтений
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<List<PreferenceResponse>>> GetCustomersAsync()
        {
            var preferences = await _preferenceRepository.GetAllAsync();
            List<PreferenceResponse> results = new List<PreferenceResponse>();
            foreach (var item in preferences)
            {
                results.Add( new PreferenceResponse()
                {
                    Name = item.Name,
                    Id = item.Id,
                });
            }
            return Ok(results);
        }
    }
}