﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerRepository _customerRepository;
        public CustomersController(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        /// <summary>
        /// получение списка клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();
            List<CustomerShortResponse> result = new List<CustomerShortResponse>();
            foreach (var customer in customers)
            {
                result.Add(new CustomerShortResponse()
                {
                    Id = customer.Id,
                    Email = customer.Email,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                });
            }
            return Ok(result);
        }

        /// <summary>
        /// получение клиента вместе с выданными ему промомкодами
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();
            CustomerResponse record = new CustomerResponse()
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
               };
            record.Preferences = customer.CustomerPreferences.Count > 0 ? customer.CustomerPreferences.Select(p => new PreferenceResponse()
            {
                Id = p.Preference.Id,
                Name = p.Preference.Name
            }).ToList() : new List<PreferenceResponse>();
            return Ok(record);
        }

        /// <summary>
        /// создание нового клиента вместе с его предпочтениями
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            if (request is null)
            {
                return BadRequest("Requred field is empty");
            }
            var customer = new Customer()
            {
                Id = Guid.NewGuid(),
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
            };
            if (request.PreferenceIds.Count > 0)
            {
                customer.CustomerPreferences = new List<CustomerPreference>();
                foreach (var item in request.PreferenceIds)
                {
                    customer.CustomerPreferences.Add(new CustomerPreference
                    {
                        PreferenceId = item,
                        CustomerId = customer.Id
                    });
                }
            }
            var data = await _customerRepository.AddAsync(customer);
            return Ok(data);          
        }

        /// <summary>
        /// Обновление клиента
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            if (request is null)
            {
                return BadRequest("Requred field is empty");
            }
            var exsistingRecord = await _customerRepository.GetByIdAsyncForEdit(id);
            if (exsistingRecord is null)
            {
                return NotFound("Entity not found");
            }
            if (request.PreferenceIds.Count > 0)
            {
                foreach (var item in request.PreferenceIds)
                {
                    if (!exsistingRecord.CustomerPreferences.Any(c => c.PreferenceId == item))
                    {
                        exsistingRecord.CustomerPreferences.Add(new CustomerPreference
                        {
                            PreferenceId = item,
                            CustomerId = exsistingRecord.Id
                        });
                    }
                }
            }
            exsistingRecord.Email = request.Email;
            exsistingRecord.FirstName = request.FirstName;
            exsistingRecord.LastName = request.LastName;
            var updateRecord = await _customerRepository.UpdateAsync(exsistingRecord);
            return Ok("Success update");
        }
        /// <summary>
        /// Удаление клиента вместе с выданными ему промокодами
        /// </summary>   
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {            
            await _customerRepository.DeleteAsync(id);
            return Ok("successfull");
        }
    }
}