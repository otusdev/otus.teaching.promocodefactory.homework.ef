﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Employee> Employees => new List<Employee>()
        {
            new Employee()
            {
                Id = new Guid("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                Role = Roles.FirstOrDefault(x => x.Name == "Admin"),
                AppliedPromocodesCount = 5
            },
            new Employee()
            {
                Id = new Guid("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                Role = Roles.FirstOrDefault(x => x.Name == "PartnerManager"),
                AppliedPromocodesCount = 10
            },
        };

        public static IEnumerable<Role> Roles => new List<Role>()
        {
            new Role()
            {
                Id = new Guid("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                Name = "Admin",
                Description = "Администратор",
            },
            new Role()
            {
                Id = new Guid("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            }
        };

        public static IEnumerable<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                Id = new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                Name = "Театр",
            },
            new Preference()
            {
                Id = new Guid("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Name = "Семья",
            },
            new Preference()
            {
                Id = new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                Name = "Дети",
            }
        };

        public static IEnumerable<Customer> Customers
        {
            get
            {
                var customers = new List<Customer>()
                {
                    new Customer()
                    {
                        Id = new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                        Email = "ivan_sergeev@mail.ru",
                        FirstName = "Иван",
                        LastName = "Петров",
                    },
                     new Customer()
                    {
                        Id = new Guid("a8c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                        Email = "kuchin@mail.ru",
                        FirstName = "Сергей",
                        LastName = "Кучин",
                    }
                };
                return customers;
            }
        }

        public static IEnumerable<CustomerPreference> CustomerPreferences => new List<CustomerPreference>()
        {
            new CustomerPreference()
                    {
                        CustomerId = new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                        PreferenceId =new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c")
                    },
                    new CustomerPreference()
                    {
                        CustomerId = new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                        PreferenceId = new Guid("c4bda62e-fc74-4256-a956-4760b3858cbd")
                    },
                    new CustomerPreference()
                    {
                        CustomerId = new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                        PreferenceId = new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84")
                    },
                    new CustomerPreference()
                    {
                        CustomerId = new Guid("a8c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                        PreferenceId = new Guid("c4bda62e-fc74-4256-a956-4760b3858cbd")
                    },
                    new CustomerPreference()
                    {
                        CustomerId = new Guid("a8c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                        PreferenceId = new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84")
                    },
        };
        public static IEnumerable<PromoCode> PromoCodes
        {
            get
            {
                var promoCodes = new List<PromoCode>()
                {
                    new PromoCode()
                    {
                        Id = new Guid("8f117e72-f68b-4f97-84a6-ddca90256c6a"),
                        BeginDate = DateTime.UtcNow,
                        Code = "123",
                        EndDate = DateTime.UtcNow.AddMonths(1),
                        ServiceInfo = "some service info 1",
                        PartnerName = "ABC",
                        CustomerId = new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                        PartnerManagerId = new Guid("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                        PreferenceId = new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c")
                    },
                    new PromoCode()
                    {
                        Id = new Guid("ec5b6c60-94a8-417e-ad1a-5911d3656d0f"),
                        BeginDate = DateTime.UtcNow,
                        Code = "345",
                        EndDate = DateTime.UtcNow.AddMonths(1),
                        ServiceInfo = "some service info 2",
                        PartnerName = "DEF",
                        CustomerId = new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                        PartnerManagerId = new Guid("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                        PreferenceId = new Guid("c4bda62e-fc74-4256-a956-4760b3858cbd")
                    },
                    new PromoCode()
                    {
                        Id = new Guid("b8cc1155-4e52-475c-92c9-18183751e152"),
                        BeginDate = DateTime.UtcNow,
                        Code = "678",
                        EndDate = DateTime.UtcNow.AddMonths(1),
                        ServiceInfo = "some service info 3",
                        PartnerName = "GHI",
                        PartnerManagerId = new Guid("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                        PreferenceId = new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84")
                    },
                };

                return promoCodes;
            }
        }
    }
}