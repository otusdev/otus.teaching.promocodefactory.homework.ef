﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T: BaseEntity       
    {
        protected OtusDbContext Context { get; }
        protected readonly DbSet<T> DbSet;

        protected IEnumerable<T> Data { get; set; }

        public EfRepository(OtusDbContext context)
        {
            Context = context;
            DbSet = Context.Set<T>();
        }

        public async Task<ICollection<T>> GetAllAsync()
        {
            return await DbSet.AsNoTracking().ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await DbSet.AsNoTracking().FirstOrDefaultAsync(e => e.Id == id);

        }
        public virtual async Task<T> GetByIdAsyncForEdit(Guid id)
        {
            return await DbSet.FirstOrDefaultAsync(e => e.Id == id);

        }
        public async Task<Guid> AddAsync(T entity)
        {
            await DbSet.AddAsync(entity);
            await Context.SaveChangesAsync();
            return entity.Id;
        }

        public async Task<T> UpdateAsync(T entity)
        {           
            DbSet.Update(entity);
            await Context.SaveChangesAsync();
            return entity;

        }
        public async Task DeleteAsync(Guid id)
        {
            var record = await Context.Set<T>().FindAsync((object)id);
            Context.Set<T>().Remove(record);
            await Context.SaveChangesAsync();
        }
        public async Task CreateList(ICollection<T> entities)
        {
            await DbSet.AddRangeAsync(entities);
            await Context.SaveChangesAsync();
        }
    }
}