﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{    
    public class InMemoryRepository<T> //: IRepository<T> //не планируется использовать
        where T: BaseEntity
    {
        protected ICollection<T> Data { get; set; }

        public InMemoryRepository(ICollection<T> data)
        {
            Data = data;
        }
        
        public Task<ICollection<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }
        public Task<Guid> AddAsync(T entity)
        {
            Data.Add(entity);
            return Task.FromResult(entity.Id);
        }

        public Task<T> UpdateAsync(T entity)
        {
            T item = Data.FirstOrDefault(x => x.Id == entity.Id);
            item = entity;
            return Task.FromResult(entity);
        }

        public Task DeleteAsync(Guid id)
        {
            T t = Data.FirstOrDefault(x => x.Id == id);
            if (t != null)
            {
                Data = (ICollection<T>)Data.Where(x => x.Id != id);
            }
            return Task.FromResult(true);
        }
    }

}