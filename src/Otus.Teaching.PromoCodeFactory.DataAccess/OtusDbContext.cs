﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class OtusDbContext : DbContext
    {
        public OtusDbContext(DbContextOptions<OtusDbContext> options) : base(options) { }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<CustomerPreference> CustomerPreferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Employee>().HasKey(r => r.Id);
            modelBuilder.Entity<Employee>().Property(c => c.FirstName).HasMaxLength(100);
            modelBuilder.Entity<Employee>().Property(c => c.LastName).HasMaxLength(100);
            modelBuilder.Entity<Employee>().Property(c => c.Email).HasMaxLength(100);

            modelBuilder.Entity<Customer>().HasKey(r => r.Id);
            modelBuilder.Entity<Customer>().Property(c => c.FirstName).HasMaxLength(100);
            modelBuilder.Entity<Customer>().Property(c => c.LastName).HasMaxLength(100);
            modelBuilder.Entity<Customer>().Property(c => c.Email).HasMaxLength(100);

            modelBuilder.Entity<Preference>().HasKey(r => r.Id);
            modelBuilder.Entity<Preference>().Property(p => p.Name).HasMaxLength(100);
            modelBuilder.Entity<Preference>().Property(p => p.ShortDescription).HasMaxLength(100);            

            modelBuilder.Entity<PromoCode>().HasKey(r => r.Id);
            modelBuilder.Entity<PromoCode>()
                 .HasOne<Customer>(pc => pc.Customer)
                 .WithMany(c => c.PromoCodes)
                 .HasForeignKey(pc => pc.CustomerId)
                 .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<PromoCode>().Property(pc => pc.Code).HasMaxLength(100);
            modelBuilder.Entity<PromoCode>().Property(pc => pc.ServiceInfo).HasMaxLength(100);
            modelBuilder.Entity<PromoCode>().Property(pc => pc.PartnerName).HasMaxLength(100);

            modelBuilder.Entity<CustomerPreference>()
               .HasKey(ab => new { ab.PreferenceId, ab.CustomerId });

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(c => c.Customer)
                .WithMany(c => c.CustomerPreferences)
             .HasForeignKey(c => c.CustomerId);

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(c => c.Preference)
                .WithMany(c => c.CustomerPreferences)
                .HasForeignKey(c => c.PreferenceId);           
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo(Console.WriteLine, LogLevel.Information);
        }

    }
}

