﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public static class DataSeed
    {
        public static async void SeedData(OtusDbContext dbContext)
        {
            await dbContext.Employees.AddRangeAsync(FakeDataFactory.Employees);
            await dbContext.Preferences.AddRangeAsync(FakeDataFactory.Preferences);
            await dbContext.Customers.AddRangeAsync(FakeDataFactory.Customers);
            await dbContext.CustomerPreferences.AddRangeAsync(FakeDataFactory.CustomerPreferences);
            await dbContext.PromoCodes.AddRangeAsync(FakeDataFactory.PromoCodes);
            await dbContext.SaveChangesAsync();           
        }
    }

}

